﻿$emulationstationLink = "https://emulationstation.org/downloads/releases/emulationstation_win32_latest.exe"
$dolphinLink = "https://dl-mirror.dolphin-emu.org/5.0/dolphin-x64-5.0.exe"
$RetroarchLink = "http://buildbot.libretro.com/stable/1.8.4/windows/x86_64/RetroArch-x64-setup.exe"

Invoke-WebRequest $emulationstationLink -OutFile "emulationstation_win32_latest.exe"
Start-Process .\emulationstation_win32_latest.exe /S

$systems = "gc","n64","nes","snes", "sega32x", "genesis", "gbc","gba","wii"

mkdir "$HOME\.emulationstation\roms"
mkdir "$HOME\.emulationstation\systems"

foreach ($system in $systems){
    mkdir "$HOME\.emulationstation\roms\$system"

}

Invoke-WebRequest $dolphinLink -OutFile "dolphin.exe"
Start-Process .\dolphin.exe -ArgumentList "/S", "/D=$HOME\.emulationstation\systems\dolphin" 

Invoke-WebRequest $RetroarchLink -OutFile "RetroArch-x64-setup.exe"
Start-Process .\RetroArch-x64-setup.exe -ArgumentList "/S","/D=$HOME\.emulationstation\systems\retroarch"

$escontent = @"
<!-- This is the EmulationStation Systems configuration file.
All systems must be contained within the <systemList> tag.-->
 
<systemList>
        <system>
                <name>nes</name>
                <fullname>Nintendo Entertainment System</fullname>
                <path>~\.emulationstation\roms\nes</path>
                <extension>.nes .NES</extension>
                <command>$home\.emulationstation\systems\retroarch\retroarch.exe -L "$home\.emulationstation\systems\retroarch\cores\fceumm_libretro.dll" "%ROM_RAW%"</command>
                <platform>nes</platform>
                <theme>nes</theme>     
        </system>
 
        <system>
                <name>snes</name>
                <fullname>Super Nintendo Entertainment System</fullname>
                <path>~\.emulationstation\roms\snes</path>
                <extension>.smc .sfc .SMC .SFC</extension>
                <command>$home\.emulationstation\systems\retroarch\retroarch.exe -L "$home\.emulationstation\systems\retroarch\cores\snes9x_libretro.dll" "%ROM_RAW%"</command>
                <platform>snes</platform>
                <theme>snes</theme>    
        </system>
 
        <system>
                <name>n64</name>
                <fullname>Nintendo 64</fullname>
                <path>~\.emulationstation\roms\n64</path>
                <extension>.v64 .z64</extension>
                <command>$home\.emulationstation\systems\retroarch\retroarch.exe -L "$home\.emulationstation\systems\retroarch\cores\mupen64plus_next_libretro.dll" "%ROM_RAW%"</command>
                <platform>n64</platform>
                <theme>n64</theme>     
        </system>
 
        <system>
                <name>genesis</name>
                <fullname>Sega Genesis</fullname>
                <path>~\.emulationstation\roms\sega32x</path>
                <extension>.bin</extension>
                <command>$home\.emulationstation\systems\retroarch\retroarch.exe -L "$home\.emulationstation\systems\retroarch\cores\genesis_plus_gx_libretro.dll" "%ROM_RAW%"</command>
                <platform>genesis</platform>
                <theme>genesis</theme> 
        </system>
 
        <system>
                <name>gbc</name>
                <fullname>Game Boy Color</fullname>
                <path>~\.emulationstation\roms\gbc</path>
                <extension>.gbc .zip</extension>
                <command>$home\.emulationstation\systems\retroarch\retroarch.exe -L "$home\.emulationstation\systems\retroarch\cores\gambatte_libretro.dll" "%ROM_RAW%"</command>
                <platform>gbc</platform>
                <theme>gbc</theme>     
        </system>
 
        <system>
                <name>gba</name>
                <fullname>Game Boy Advance</fullname>
                <path>~\.emulationstation\roms\gba</path>
                <extension>.gba</extension>
                <command>$home\.emulationstation\systems\retroarch\retroarch.exe -L "$home\.emulationstation\systems\retroarch\cores\vbam_libretro.dll" "%ROM_RAW%"</command>
                <platform>gba</platform>
                <theme>gba</theme>     
        </system>
 
 
        <system>
                <name>gc</name>
                <fullname>Nintendo GameCube</fullname>
                <path>~\.emulationstation\roms\gc</path>
                <extension>.iso .ISO .gcm</extension>
                <command>$home\.emulationstation\systems\dolphin\Dolphin.exe --batch --exec="%ROM_RAW%"</command>
                <platform>gc</platform>
                <theme>gc</theme>      
        </system>
 
        <system>
                <name>wii</name>
                <fullname>Nintendo Wii</fullname>
                <path>~\.emulationstation\roms\wii</path>
                <extension>.iso .ISO</extension>
                <command>$home\.emulationstation\systems\dolphin\dolphin.exe -e "%ROM_RAW%"</command>
                <platform>wii</platform>
                <theme>wii</theme>     
        </system>

</systemList>
"@

Set-Content -Path "$home\.emulationstation\es_systems.cfg" -Value $escontent

Copy-item "./retroarch-Cores/*" -Destination "$HOME\.emulationstation\systems\retroarch\cores"

Remove-item emulationstation_win32_latest.exe
Remove-item dolphin.exe
Remove-item RetroArch-x64-setup.exe
